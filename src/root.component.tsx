import React from "react";
import { Navbar, Nav } from "react-bootstrap";

import "bootstrap/dist/css/bootstrap.min.css";

export default function Root(props) {
  const theme = "dark";
  return (
    <section>
      <Navbar bg={theme}>
        <Navbar.Brand>
          <Nav.Link href="#">Fish</Nav.Link>
        </Navbar.Brand>
      </Navbar>
      <br />
      <Navbar bg={theme}>
        <Navbar.Brand>
          <Nav.Link href="#">Dogs</Nav.Link>
        </Navbar.Brand>
      </Navbar>
      <br />
      <Navbar bg={theme}>
        <Navbar.Brand>
          <Nav.Link href="#">Cats</Nav.Link>
        </Navbar.Brand>
      </Navbar>
      <br />
      <Navbar bg={theme}>
        <Navbar.Brand>
          <Nav.Link href="#">Reptiles</Nav.Link>
        </Navbar.Brand>
      </Navbar>
      <br />
      <Navbar bg={theme}>
        <Navbar.Brand>
          <Nav.Link href="#">Birds</Nav.Link>
        </Navbar.Brand>
      </Navbar>
    </section>
  );
}
